public class Tipos {
    public static void main(String ...args) {
        byte a = 2;
        byte b = 8;

        //cating
        byte c= (byte) (a + b);

        System.out.println(c);
    
        //primer fomra de catear float 
        float x = (float) 15.5;
        float w = (float) 20.3;
        float z = x + w;

        System.out.println(z);

        //casting segunda forma

        float q = 20.3f;
        float r = 40.2f;

        float p = q + r;

        System.out.println(p);


        // cating tercera forma 

        int o = 5;
        int s = 3;
        float n = (float) o / (float) s;

        System.out.println(n);
    }
    
}
