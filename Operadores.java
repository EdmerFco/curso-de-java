public class Operadores {
    public static void main(String[] args) {
        // Asigancion = 
      //  int a = 2;
        int b;
        b = 2 + 2 +3;
        System.out.println("Resultado de B" + b );

        //preincremento

        int p  = 0;
        ++p;
        System.out.println("valir de P "+ p);

        //Posincremento
        int y = 0;
        y++;
        System.out.println("este es el valor de Y" + y);

        //Preincremento vs Postincremento 

        int q = 3;
        int w = 2;
        int e = q * ++w;

        System.out.println("Preincremento");
        System.out.println(q);
        System.out.println(w);// en este caso se primero se le suma 1 y depues se hace la  multiplicacion
        System.out.println(e);

        //postincremento 
        System.out.println("Postincremento");
        e = q *  w++;
        System.out.println(q);
        System.out.println(w);// en este caso primero se multiplica y depues se le suma 1 a la variable
        System.out.println(e);

        //Recomendacion:
        // Nunca utilicen preincremento o postincremento dentro de una asiganacion. 
        
        //ejemplo de como usarlo correctamente 
        //b++;
        //int c = a * b;
        // o asi:
        //++b;
        //int c = a * b;

        // el motivo es una buen paractica para el programdor pueda leer mas facil el codigo. 

        //Operadores Aritmeticos. 
        //+ - * / 
        //Operador . (POO)
        //Operador () en donde primero se resolveran las operaciones que este dentro del los () y depues las que esten afuera con el orden jerarquico
        //Jerarquia: 
        // Primero se resuleve las * / (con prioridad de Izquierda a derecha)
        //segundo se resulven las + -  (con prioridad de Izquierda a derecha) 
         int r = 3 + 3 * 3 - 3;
         System.out.println("Resultado de R " + r);

         // Operadores Logicos
         // nos permite relisar operaciones para validar si son siertas o no lo son (TRUE O FALSE)
         // Operadores logicos Y O 
         // Tabla de Verdad 
         // Tablas de verdad de Y (&&)
         //   A        B     Resultado 
         // true      true    true 
         // true      False   False 
         // False     true    False 
         // False     False   False 
         int edad = 9;
         boolean niñez = edad <= 11 && edad >=0;
        System.out.println("este es el resultado " + niñez);

         // Tabla de verdad de O (||)
         // A      B     Resultado 
         // True   True   True 
         // True   False  True
         // False  True   True
         // False  False  False

     }
}