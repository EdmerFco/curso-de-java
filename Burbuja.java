

/**
 * Burbuja
 */
public class Burbuja {

    /**
     * @param args
     */
    public static void main(String[] args) {
        //Declaracion de arreglo
        int a[] = new int[10];

        //poblacion del arreglo con numeros aleatorios 
        for(int i = 0; i  < a.length; i++){
            a[i] = (int) (Math.random() * 99);
        }

        //Mostrar el areglo por consola
        for(int v : a){
            System.out.print(" " + v);
            
        }
        System.out.println();
        //Orderna por el metodo Burbja

        //1.Recorrer el arreglo como tantas veces contenga menos 1. // si el areglo tiene 10 elementos se recorren 9 veces
        //2 en cada recorrido comparo el elemento actual con el elemeneto de seguimiento para saber si estan ordenados si no lo estan los ordeno

        // original: 5, 6, 9, 1, 14, 12
        //1.         5, 6, 1, 9, 12, 14
        //2.         5, 1, 6, 9, 12, 14
        //3.         1, 5, 6, 9, 12, 14
        //4.         1, 5, 6, 9, 12, 14
        //5.         1, 5, 6, 9, 12, 14



        //Codigo

        for(int recorrido = 0; recorrido < a.length-1; recorrido++){
            for(int elemento = 0; elemento < a.length-1; elemento++){
                if(a[elemento] > a[elemento + 1]){
                    int t = a[elemento];
                    a[elemento] = a[elemento + 1];
                    a[elemento + 1 ] = t;
                }
            }
        }

        //mostrar el areglo ya ordenado
        for(int v : a){
            System.out.print(v + " ");
        }

        System.out.println();
    }
}