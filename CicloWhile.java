public class CicloWhile {
    public static void main(String[] args) {
        //While(Expresion booleana){
            //sentencia
        //}

        //break
        //continue

        int i = 0;
        while(i < 10){
            System.out.println(i); 
            i++;
        }

        //como funciona le break en el while 
        System.out.println("Como funciona break en el while");
        int a = 0;
        
        while(a < 10){
            System.out.println("este el valor de A: " + a);
            a++;
            if(a == 7){
                break;
            }
        }
          //como funciona le continue en el while 
          System.out.println("Como funciona break en el continue");
          int b = 0;
          while(b < 10){
            System.out.println("Este el valor de B " + b);
            b++;
            if(b % 2 == 0){
                System.out.println(b + " Es un numero par");
                continue;
            }
            System.out.println("El valor despues del Conitnue es: " + b);
          }
    }
}
