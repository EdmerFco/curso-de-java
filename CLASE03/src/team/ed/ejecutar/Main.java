package team.ed.ejecutar;

//import team.ed.herencia.Persona;
import team.ed.herencia.Profesor;

public class Main {
    public static void main(String[] args) {

        //ya no se puede instanciar la clase por la palabra reservada abstract
        // Persona persona = new Persona();
        // persona.nombre = "Pedro";
        // persona.hablar();
        // persona.comer();

        //Se hace un buen uso de la herncia por que se esta creando una clase profesor que herada las caracterisitcas de otra y aprte de utilizarlos se le agrega nuevos metodos
        Profesor profesor = new Profesor("Puerco");
       // profesor.nombre = "edmer";
        profesor.hablar();
        profesor.ensenar();
        profesor.ensenar("Programacion");
        profesor.reir();
    }
}
 