package team.ed.ejecutar;

import team.ed.formas.Circulo;
import team.ed.formas.Figura;
import team.ed.formas.Rectangulo;
import team.ed.formas.Triangulo;

public class PruebaFormas {
    public static void main(String[] args) {
        //instanciado las clases es una forma e poder usar los metodos con la interfas
        // Rectangulo r = new Rectangulo(20, 30);
        // Circulo c = new Circulo(15);
        // Triangulo t = new Triangulo(10, 20);
        // System.out.println("Rectangulo:" + r.getArea());
        // System.out.println("Circulo:" + c.getArea());
        // System.out.println("Tringulo:" + t.getArea()  );

        //Se cambia el 
        Figura formas[] = new Figura[3];
        formas[0] = new Rectangulo(20, 30, "Rectangulo");
        formas[1] = new Circulo(15, "Circulo");
        formas[2] = new Triangulo(10, 20, "Triangulo");

        for(Figura v : formas){
            System.out.println("El Area de " + v.getNombre() + ": " + v.getArea() );
        }

    }
    
}
