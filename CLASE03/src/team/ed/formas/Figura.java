package team.ed.formas;

public interface Figura {
    // solo puede tener atributos constantantes que nunca van a cambiar 
    double PI = Math.PI;
    
    // todas las clases que use la interface Figura tiene que usar los metodos que esten en la inteface si no no van a poder utilizar la interfas
    double getArea();
    double getPerimetro(); 

    String getNombre();
}
