package team.ed.formas;

public class Circulo implements Figura{
    String nombre;
    double radio;

    public Circulo(double radio, String nombre){
        this.nombre = nombre;
        this.radio = radio;
    }
    @Override
    public double getArea() {
       return PI * this.radio * this.radio;
    }

    @Override
    public double getPerimetro() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'getPerimetro'");
    }
    @Override
    public String getNombre() {
        return this.nombre;
    }
    
}
