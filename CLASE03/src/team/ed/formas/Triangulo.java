package team.ed.formas;

public class Triangulo implements Figura{
    String nombre;
    double base;
    double altura;


    public Triangulo(double base, double altura, String nombre){
        this.nombre = nombre;
        this.base = base;
        this.altura = altura;
    }

    @Override
    public double getArea() {
       return (this.base * this.altura) / 2;
    }

    @Override
    public double getPerimetro() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'getPerimetro'");
    }

    @Override
    public String getNombre() {
        return this.nombre;
    }
    
}
