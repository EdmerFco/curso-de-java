package team.ed.formas;

// la clase Cuadrado esta usando la interfas de Figura 
public class Rectangulo implements Figura{
//es obligatorio darles un comportamiento los metodos para poder usar la interface de Figura 

String nombre;
double alto;
double ancho;
public Rectangulo(double alto, double ancho, String nombre ){
    this.nombre = nombre;
    this.alto = alto;
    this.ancho = ancho;
}
    @Override
    public double getArea() {
        return this.alto * this.ancho;
    }

    @Override
    public double getPerimetro() {
        // TODO Auto-generated method stub
        throw new UnsupportedOperationException("Unimplemented method 'getPerimetro'");
    }
    @Override
    public String getNombre() {
        return this.nombre;
    }

    
    
}
