package team.ed.herencia;

     //Adtraccion
     //Clases abstractas: cuando se habla de clases abtractas estamos hablando 
     // se debe agregar la palrabra reservada abstract 
public abstract class Persona {
   public String nombre;
   public int edad;
   public float estatura;
   public Float peso;

   public void hablar(){
    System.out.println("Hola Soy: " + this.nombre);
   }

   public void comer(){
    System.out.println("Estoy comiendo" );
   }
   //un cosntructor es un metodo especial al cual no se le tiene que decir la que va a devolver y tiene que llamarse igual a la clase 
   public Persona(){
      this.nombre = "Sin nombre";
   }
   public Persona(String n){
      this.nombre = n;
   }

   public abstract void reir();
}
