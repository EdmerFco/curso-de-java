package team.ed.herencia;

public class Profesor  extends Persona{

// como buena practica se agrega la palabra reservada super()
//la palabra reservada super hace referencia al constructor de la clase que se esta heredando 
    public Profesor(){
        super();
    }

    public Profesor(String nombre ){
        super(nombre);
    }


    public void ensenar(){
        System.out.println("Estoy enseñando");
    }

    // Overloading / sobre carga de metodos
    //Override / Sobreescritura de metodos 

    //Sobrecarga de metodos: 
    //Perite crera varios metodo cob ek musmo nombre siempre y cuabdo la firma es diferente. 
    //se logra cambiando el orden de los parametros y/o la cantidad de los mimos 
    //la sobrecarga de metodos se hace en la misma clase 
    //LA sobre carga de metodos se ve mucho en los constructores 
    String materias;

    public void ensenar(String n){
        System.out.println("Estoy enseñando " + n);
    }

    public void ensenar(String n, int horas){
        System.out.println("Estoy enseñando " + n);
    }

     //Override / Sobreescritura de metodos 
     // permite cambiar el comportamiento de un metodo 
     // pero se hace sobre la erencia o el polimorfismo 

     //@Override cambia el comportamiento del metodo que se esta heredando 
     //si no tiene la palabra reservada @Override  no se esta sobrescribiendo el metodo si no que se esta creando uno nuevo
     @Override
     public void comer(){
        System.out.println("Estoy comiendo, no cagues el palo ");
     }

     //se tiene que declara el metodo rair ya que si no dara un error esto por el metodo reir al cual se escribio la  palabra reservada abstract que se agrego con la clase persona 
     @Override
     public void reir(){
        System.out.print("Me rio a carcajadas ");
     }
}
