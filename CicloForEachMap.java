import java.util.HashMap;
import java.util.Map;

public class CicloForEachMap {
    public static void main(String[] args) {
        Map<Integer, String> nombres = new HashMap<>();
        nombres.put(1, "jose");
        nombres.put(2, "Beto");
        nombres.put(3,"Pedro");
        nombres.put(4,"Francisco");
        nombres.put(5, "Alejandro");

        nombres.forEach((id, name) ->{
            System.out.println("ID: "+ id);
            System.out.println("Nombre " + name);
        });
    }
}
