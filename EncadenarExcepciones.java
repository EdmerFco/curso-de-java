import java.io.EOFException;

/**
 * Lanzar excepciones a metodos superiores
 */

 // throws permite elevar una excepcion a un metodo superior 
 //throw lanza la excepxion 


 //Excepciones verficadas: Controlar el usu de try cath en tiempo de compilacion 
 //Es obligatori usar el try catch para poder compilar 
 //son aquellas que heredan de exception 

 //Excepciones no verificadas: no obligan el uso de try cath en timepo de compilacion 
 //son las que  Heredan RuntimeException
public class EncadenarExcepciones {
    public static void main(String[] args) {
        int a = 6;
        int b = 0;
        try {
            int c = dividir(a, b);
            System.out.println(c);
        } catch (ArithmeticException e) {
           System.out.println("No se puede devidir por 0");
        }
        System.out.println("Este porgrama puede seguir funcionando");
        try{
            Superior();
        }catch(Exception e){
            e.printStackTrace();
        }
        // Superior(); no se puede lazar el metodo si en el try catch

        System.out.println("Programa finalizado totalmente");
    }

    public static int dividir(int a, int b) throws ArithmeticException{
        return a / b; 
    }

    public static void Superior() throws Exception{
        try {
             intermedio();
        } catch (Exception e) { 
            throw new Exception("Metodo Superior", e);
        }
    }

    public static void intermedio() throws Exception{
        try {
             inferior();
        } catch (Exception e) {
            throw new Exception("Metodo intermedio", e);
        }
    }

    public static void inferior() throws Exception{
        throw new Exception("Metodo inferior ");
    }
}