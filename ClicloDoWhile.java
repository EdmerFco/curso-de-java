public class ClicloDoWhile {
    public static void main(String[] args) {
        // El siclo do while siempre va a ejecutar por lo menos una vez el codigo dentro
        // do while 
        // El codigo se ejecutara por lo mens una vez 

        //do {
            //sentencia
        //}while(Condicional)
        //break: Deteine el ciclo
        //continue: no va a ejecutar el codigo del bloque del ciclo
        //que este luego de la palabra reservada
        int i = 0; 

        do{
            System.out.println(i);
            i++;
        }while(i <10);

        //como funciona break 

        int a = 0;

        do{
            System.out.println("Como funciona el breck en el do while "+ a);
            a++;
            if(a == 7){
                break;
            }
        }while(a < 10);

        //Como funciona el continue en el cilo do while

        int b = 0;
        do{
            System.out.println("la variable vale " + b);
            b++;
            if(b == 7){
                continue;
            }
            System.out.println("Estoy despues del continue: " + b);
        }while(b < 10);


        int c = 0;

        do{
            System.out.println("Estes es el valor de C: " + c);
            c++;
            if(c % 2 ==0){
                System.out.println(c + "Es un numero Par");
                continue;
            }
            System.out.println("El valor despues del continue es de: " + c);
        }while(c <10);
    }
}