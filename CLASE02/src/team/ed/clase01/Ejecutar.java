package team.ed.clase01;
import team.ed.clase02.Persona;

public class Ejecutar {
    public static void main(String[] args) {
        Persona p = new Persona();

        p.edad = 23;
        System.out.println(p.edad);
        //la varoable sexo no tiene modificardor es por eso que no se puede usar fuera de otros paquetes
        //p.Sexo = "Masculino";
        //System.out.println(p.Sexo);
        
        //En este caso da erroe por que no se esta intentado llegar a la clase des de otro paquete y no se puede hacer
        //Pero si se puede ejecutar el metodo saluda1() por que tiene el modificador en public
        Estudiante02 e = new Estudiante02();
        //  e.telefono = "44112168"; muestra error
        e.setTelefono("44115188");
        e.saludar();
    
    }
}
