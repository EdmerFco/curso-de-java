package team.ed.clase01;

import team.ed.clase02.Persona;

public class Estudiante02 extends Persona{

    public void setTelefono(String t){
        telefono = t;
    }

    public void saludar(){
        System.out.println("Hola, mi telefono es: " + telefono);
    }
}
