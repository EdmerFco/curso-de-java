package team.ed.clase02;

public class Matematicas {
    public static float PI = 3.1416f;
    // se usa la palabra reservada static para ya no instanciar la clase completa 
    public static int suma(int a, int b){
        return a + b;
    }
}
