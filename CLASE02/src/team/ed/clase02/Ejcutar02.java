package team.ed.clase02;

import team.ed.clase01.Estudiante02;

public class Ejcutar02 {
    public static void main(String[] args) {
        Persona persona = new Persona();


        persona.edad = 35;
        // persona.nombre = "Edmer Fco"; no se pude llegar al istancia por nombre tiene un acceso privado en persona
        // nisiquiera estan dentro del mismo paquete solo dentro de su clase  

        //no tiene ningun modificar es por ello que se puede instanciar la variable sexo en el mismo paquete y clase 
        persona.Sexo = "Masculino";

        System.out.println("Esta es la esas de la persona " + persona.edad);
        System.out.println("Esta es el Sexo de la persona " + persona.Sexo);

        //Ejemplo con un clase heredada sin los modificadores 
        Estudiante e = new Estudiante();
        e.Sexo = "Masculino";
        System.out.println(e.Sexo);
        // se esta llegando a la variable Sexo desde una clase que hereda dentro del mismo paquete
        //estudiantes es una clase heredada de la la clase Persona  
        e.saludar();


         //Ejemplo con un clase heredada con el modificador protected
         //en este caso el modificardpr protected pemite acceder a las variables telefono caso contrario si no hay modificado al declararla 
         Estudiante02 e2 = new Estudiante02();
         e2.telefono = "4411512168";
         e2.saludar();

    }
}
