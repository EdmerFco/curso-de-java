public class EntradaDiscoteca {
    public static void main(String[] args) {
        int edad = 17;
        try {
            validar(edad);
        } catch (ExcepcionMayotDeEdad emde) {
            System.out.println(emde);
        }
     
        System.out.println("Proceso teminado");
    }
    public static void validar(int edad) throws ExcepcionMayotDeEdad{
        if(edad <18){
            throw new ExcepcionMayotDeEdad("no te puedo dejar entrar");
        }else {
            System.out.println("Bienvenido a la disco");
        }
    }
}
