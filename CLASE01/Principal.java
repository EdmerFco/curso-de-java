public class Principal {
    public static void main(String[] args) {
        
        //se instanse la clase persona
        Persona persona = new Persona();
        //Otro instancia con la sobrecarga 
        Persona otraPersona = new Persona("Leidy Gonzalez");
        //Otro instnaci con la sobrecarga
        Persona tercerPersona = new Persona("Francisco", 23);

        //se asigna el valor al tipo de dato persona
          // persona.nombre = "Edmer Fco"; 
          // persona.edad = 23;
          // persona.peso = 69f;
          // persona.estatura = 1.6f;

          //Asignar valores con metodos en las clases
          persona.setNombre("Edmer fco");
          persona.setEdad(23);

        //Se mustran en consola los valores dado 
        System.out.println("Nombre: " + persona.getNombre());
        System.out.println("Edad: " + persona.getEdad());
        // System.out.println("Peso: " + persona.peso);
        // System.out.println("Estatura: " + persona.estatura);

        System.out.println("Nombre de la otra persona " +  otraPersona.getNombre());

        System.out.println("Nombre del Tercer persona " + tercerPersona.getNombre());
        System.out.println("Edad  del Tercer Persona " + tercerPersona.getEdad());

        //metodo de saludar 

        // persona.saludar();
        // otraPersona.saludar();
        // tercerPersona.saludar(); 
    }
}