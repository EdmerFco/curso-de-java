
// un abuena pratica para crear las classe es como se crea el orden seria el siguinete 
//1. los atributos
//2. constructores
//3. metodos set y get
public class Persona {
    //Se nesesitan los get y set para entrar a loas propiedades privadas  

        // Atributos 
        //se crean los atributos con la palbra reservada private 
        private String nombre;
        private int edad;
        private float peso;
        private float estatura;

        //constructor
        //El constructor es un metodo especial por que no es nesesario decile que devuelva algo
        // cl constructor tiene que llevar el nombre de la clase 
        public Persona(){
            // Sirve para inicialisar los valores por defecto de un objeto instanciado
            this.nombre = "";
            this.edad = 0;
            this.edad = 0; 
            this.peso = 0f;
        }
        // puede tener los constructores que tu quieras
        //sobrecarga: es escribir dos metodos con el mismo nombre pero con diferente firma
        public  Persona(String n){
            this.nombre = n;
            this.saludar();
        }

        //El tercer constructor
        public Persona(String n, int e){
            this.nombre = n;
            this.edad = e;
        }

        //Metodos: son los comportamientos que optiene las clases 
        //Getter y Setter
        //Get obtener valores
        //set Asiganar valores


        //con el set se asigana valor 
        // se le llama firma a la palabra reservada que inicia el metodo, en el caso de los set no devulve nada es por eso que se le agrega la plabra reservada void
        // en la mayoria de los set no se devulve valor es por eso que tien la palabra reservada Void 

         void setNombre(String n ){
            this.nombre = n; 
        }

        //con el get optenemos los valores
        //para la palabra reservada de del get tiene que ser del mismo tipo de dato valor al que sera retornado,
        // la firma se tiene que decirnos que nos va a devolver, en este caso seria un valor de tipo string
        String getNombre() {
            return this.nombre;
        }

        void setEdad(int e){
            if(e <0 || e > 150){
                System.out.println("la Edad no es la correcta");
            }else {
                this.edad = e;
            }
        }

        int getEdad(){
            return edad;
        }
        
        void saludar(){
            System.out.println("Hola, mi nombre es: " + this.nombre);
        }
} 
