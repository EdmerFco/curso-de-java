//Las Excepciones nos sirve para controlar errores poco frecuentes


/*Estructura de menejo de excepcones 
 * try{
 *  en este bloque se coloca el codigo que puede tener errores en tiempo de ejecucion 
 * }
 * catch(Exception e){
 *  Aqui es donde manejamos la excepcion 
 * }finally{
 * En este bloque colocaremos lo que quereamos que se ejecute pase o no pase una excepcion 
 * }
 */

 import java.util.Scanner;
public class ExcepcionDivisionPorCero {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Programa para dividir");
        System.out.println();

        int numerador, denominador, resultado; 
        System.out.print("Digita el numerador: ");
        numerador = sc.nextInt();
        System.out.print("Digita el denominador: ");
        denominador = sc.nextInt();

        try {
            resultado = numerador / denominador;
            System.out.println("Resultado: " + resultado);
            //si hubiera masliena, no se ejecutan si ocurre una excepcion
        } catch (Exception e) {
            System.out.println(e);
            System.out.println("No se puede devidir por 0");
        }finally{
            System.out.println("Programa teminado");
        }

       
    }
}
