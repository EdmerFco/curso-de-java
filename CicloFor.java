public class CicloFor{
    public static void main(String[] args) {
        //for (inicializacion; exprecion booleana; incrementeto){
            //sentencia
        //}
        //break
        //continue

        for(int i = 0; i <10; i++){
            System.out.println("este es el valor de I: " + i);
        }

        //como funciona el break en un for

        for(int a =1; a< 10; a ++){
            System.out.println("Este es el valor de A: " + a);
            if(a % 2 == 0){
                break;
            }
        }
        //como funciona el continue
        for( int b = 0; b < 10; b++ ){
            System.out.println("Este es el valor de B " + b);

            if(b % 2 == 0){
                continue;
            }
            System.out.println("depues del consecutivo se ejecuta esta linea");
        }

    }
}